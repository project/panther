<?php

declare(strict_types=1);

namespace Drupal\Tests\panther\FunctionalJavascript;

use Drupal\user\UserInterface;
use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\StaleElementReferenceException;
use Facebook\WebDriver\WebDriver;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverElement;
use Facebook\WebDriver\WebDriverKeys;

/**
 * Provides Drupal-related methods.
 */
trait DrupalTrait {

  /**
   * Logs in as a user.
   *
   * @param string $user
   *   The user's name.
   * @param string $password
   *   The password.
   */
  public function loginAs(string $user, string $password): void {
    $this->goToPage('/user/login');
    $this->maximizeWindow();

    self::assertPageContains('Log in');

    $this->submitForm('Log in', [
      'name' => $user,
      'pass' => $password,
    ]);
  }

  /**
   * Select an operation on a table row with a given text.
   *
   * @param string $text
   * @param int $column
   * @param string $operation
   */
  public function selectOperationOnTableRowWithText(
    string $text,
    int $column,
    string $operation,
  ): void {
    $value = sprintf(
      '//table//tr[td[%d][contains(., \'%s\')]]',
      $column,
      $text
    );

    $this->scrollDownUntilVisible(WebDriverBy::xpath($value));

    $crawler = $this
      ->getClient()
      ->getCrawler();

    $row = $crawler->filterXPath($value);
    $row->filter('.dropbutton__toggle')->click();

    try {
      $this->getClient()->waitFor('.dropbutton__items');
      $row->selectLink($operation)->click();

      // Reload the page to reset the crawler state.
      $this->getClient()->reload();
    }
    catch (\Exception $e) {
      $this->fail($e->getMessage());
    }
  }

  /**
   * Select an option from a select element whose label is given.
   *
   * @param string $label
   *   The label of the select element.
   * @param string $value
   *   The value of the option to select.
   */
  public function selectOption(string $label, string $value): void {
    $crawler = $this->getClient()->getCrawler();

    $xpath = sprintf(
      ".//label[text()[contains(.,'%s')]][1]/following-sibling::select[1]//option[@value='%s']",
      $label,
      $value
    );
    $option = $crawler->filterXPath($xpath);
    $option->click();
  }

  /**
   *
   *
   * @param \Facebook\WebDriver\WebDriverBy $by
   */
  public function scrollDownUntilVisible(WebDriverBy $by): void {
    try {
      $element = $this->getClient()->findElement($by);
      while (!$element->isDisplayed()) {
        $this->getClient()->getKeyboard()->pressKey(WebDriverKeys::PAGE_DOWN);
      }
    }
    catch (\Exception $e) {
      $this->fail($e->getMessage());
    }
  }

  /**
   *
   */
  public function clickSubmitButton(): void {
    try {
      $this->getClient()->waitFor('#edit-submit');
      $this->getClient()
        ->findElement(WebDriverBy::cssSelector('#edit-submit'))
        ->click();
    }
    catch (\Exception $e) {
      $this->fail($e->getMessage());
    }
  }

  /**
   * Create a user.
   *
   * @param \stdClass $user
   */
  public function userCreate(
    \stdClass $user,
  ): void {
    $this->driver->userCreate($user);
    $this->createdEntityManager->addUser($user);
  }

  /**
   * Delete a user by email.
   *
   * @param string $email
   *   The user's email.
   */
  public function deleteUserByEmail(string $email): void {
    try {
      /** @var \Drupal\user\UserInterface[] $users */
      $users = \Drupal::entityTypeManager()
        ->getStorage('user')
        ->loadByProperties(['mail' => $email]);

      if (\count($users) > 0) {
        $user = reset($users);
        $this->driver->userDelete((object) ['uid' => $user->id()]);
        $this->driver->processBatch();
      }
    }
    catch (\Exception $e) {
      $this->fail($e->getMessage());
    }
  }

  /**
   * Create a user with a given role and fields and then log in as that user.
   *
   * @param string $role
   *   The role.
   * @param array $fields
   *   The user fields.
   */
  public function loginAsUserByRole(string $role, array $fields = []): void {
    $name = $this->driver->getRandom()->name(8);
    $password = $this->driver->getRandom()->name(16);

    $user = (object) [
      'name' => $name,
      'pass' => $password,
      'role' => $role,
    ];
    $user->mail = "{$user->name}@example.com";

    foreach ($fields as $field => $value) {
      $user->{$field} = $value;
    }

    $this->userCreate($user);

    $roles = explode(',', $role);
    $roles = array_map('trim', $roles);
    foreach ($roles as $role) {
      if (!in_array(strtolower($role), [
        'authenticated',
        'authenticated user',
      ])) {
        // Only add roles other than 'authenticated user'.
        $this->driver->userAddRole($user, $role);
      }
    }

    $this->loginAs($name, $password);
  }

  /**
   *
   *
   * @param \Drupal\user\UserInterface $user
   * @param string $password
   */
  public function setUserPassword(UserInterface $user, string $password): void {
    try {
      $user->setPassword($password)->save();
    }
    catch (\Exception $e) {
      $this->fail($e->getMessage());
    }
  }

  /**
   * @param int $timeout
   * @param string $condition
   *
   * @return bool
   */
  public function waitOnJavascript(int $timeout, string $condition): bool {
    $script = 'return (' . rtrim($condition, " \t\n\r;") . ');';

    try {
      $start = microtime(TRUE);
      $end = $start + $timeout / 1000.0;
      do {
        $result = $this->getClient()->executeScript($script);
        if ($result) {
          break;
        }
        usleep(10000);
      }
      while (microtime(TRUE) < $end);

      return (bool) $result;
    }
    catch (\Exception $e) {
      $this->fail($e->getMessage());
    }
  }

  /**
   * @param int $timeout
   *
   * @return bool
   */
  public function waitForAjaxToFinish(int $timeout = 1000): bool {
    $condition = <<<JS
    (function() {
      function isAjaxing(instance) {
        return instance && instance.ajaxing === true;
      }
      var d7_not_ajaxing = true;
      if (typeof Drupal !== 'undefined' && typeof Drupal.ajax !== 'undefined' && typeof Drupal.ajax.instances === 'undefined') {
        for(var i in Drupal.ajax) { if (isAjaxing(Drupal.ajax[i])) { d7_not_ajaxing = false; } }
      }
      var d8_not_ajaxing = (typeof Drupal === 'undefined' || typeof Drupal.ajax === 'undefined' || typeof Drupal.ajax.instances === 'undefined' || !Drupal.ajax.instances.some(isAjaxing))
      return (
        // Assert no AJAX request is running (via jQuery or Drupal) and no
        // animation is running.
        (typeof jQuery === 'undefined' || jQuery.hasOwnProperty('active') === false || (jQuery.active === 0 && jQuery(':animated').length === 0)) &&
        d7_not_ajaxing && d8_not_ajaxing
      );
    }());
JS;

    return $this->waitOnJavascript($timeout, $condition);
  }

  /**
   * Wait for a DOM element to appear.
   *
   * @param string $selector
   *   The CSS selector.
   *
   * @return \Facebook\WebDriver\WebDriverElement|null
   *
   * @throws \Facebook\WebDriver\Exception\NoSuchElementException
   * @throws \Facebook\WebDriver\Exception\TimeoutException
   */
  function waitForDomElement(string $selector): ?WebDriverElement {
    $this->getClient()->wait()->until(
      static function(WebDriver $driver) use ($selector) {
        try {
          return $driver->findElement(WebDriverBy::cssSelector($selector));
        }
        catch (StaleElementReferenceException | NoSuchElementException $e) {
          return NULL;
        }
      }
    );

    return NULL;
  }

  /**
   * Expand the toolbar.
   */
  public function expandToolbar(): void {
    $this->getClient()->getCrawler()->filter('.toolbar-menu__trigger')->click();
  }

}
