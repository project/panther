<?php

declare(strict_types=1);

namespace Drupal\Tests\panther\FunctionalJavascript;

use Drupal\Driver\DrupalDriver;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Panther\Client;

/**
 *
 */
class PantherTestCase extends TestCase {

  // Custom assertions.
  use DomCrawlerAssertionsTrait;
  use DrupalAssertionsTrait;
  use BrowserKitAssertionsTrait;

  // Custom methods.
  use DrupalTrait;

  protected DrupalDriver $driver;
  protected CreatedEntityManager $createdEntityManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $drupal_root = \getenv('DRUPAL_ROOT');
    $drupal_host = \getenv('DRUPAL_HOST');
    $this->driver = new DrupalDriver($drupal_root, $drupal_host);
    $this->driver->setCoreFromVersion();

    // Bootstrap Drupal.
    $this->driver->bootstrap();

    $this->createdEntityManager = new CreatedEntityManager($this->driver);
  }

  protected function tearDown(): void {
    parent::tearDown();

    $this->takeScreenshotIfTestFailed();

    // Build a new client instance for each test.
    self::getClient()->quit();
    self::resetClient();

    // Clean up.
    $this->createdEntityManager->deleteEntities();

    // Restore the error handler to avoid issues with PHPUnit.
    restore_error_handler();
  }

  protected static function getClient($reset = FALSE): Client {
    static $client;

    $selenium_host = \getenv('SELENIUM_HOST');
    $drupal_host = \getenv('DRUPAL_HOST');

    if (!$client || $reset) {
      $client = Client::createSeleniumClient(
        $selenium_host,
        NULL,
        $drupal_host
      );
    }

    return $client;
  }

  private static function resetClient(): void {
    self::getClient(TRUE);
  }

  public function takeScreenshotIfTestFailed(): void {
    if (class_exists(BaseTestRunner::class) && method_exists(
        $this,
        'getStatus'
      )) {
      /**
       * PHPUnit <10 TestCase.
       */
      $status = $this->getStatus();
      $isError = BaseTestRunner::STATUS_FAILURE === $status;
      $isFailure = BaseTestRunner::STATUS_ERROR === $status;
    }
    elseif (method_exists($this, 'status')) {
      /**
       * PHPUnit 10 TestCase.
       */
      $status = $this->status();
      $isError = $status->isError();
      $isFailure = $status->isFailure();
    }
    else {
      /*
       * Symfony WebTestCase.
       */
      return;
    }
    if ($isError || $isFailure) {
      $type = $isError ? 'error' : 'failure';
      $this->takeScreenshots($type, $this->toString());
    }
  }

  public function takeScreenshots(string $type, string $test): void {
    if (!($_SERVER['PANTHER_ERROR_SCREENSHOT_DIR'] ?? FALSE)) {
      return;
    }

    $screenshotPath = sprintf(
      '%s/%s_%s_%s.png',
      $_SERVER['PANTHER_ERROR_SCREENSHOT_DIR'],
      date('Y-m-d_H-i-s'),
      $type,
      strtr($test, ['\\' => '-', ':' => '_']),
    );
    $this->getClient()->takeScreenshot($screenshotPath);
    if ($_SERVER['PANTHER_ERROR_SCREENSHOT_ATTACH'] ?? FALSE) {
      printf('[[ATTACHMENT|%s]]', $screenshotPath);
    }
  }

  protected function goToPage(string $path): void {
    $this->getClient()->request('GET', $path);
  }

  protected function maximizeWindow(): void {
    $this->getClient()->manage()->window()->maximize();
  }

  protected function submitForm(string $button, array $fields): void {
    $this->getClient()->submitForm($button, $fields);
  }

  protected function clickLink(string $string): void {
    $this->getClient()->clickLink($string);
  }

  protected function clickButton(string $string): void {
    $this->getClient()->getCrawler()->selectButton($string)->click();
  }

}
