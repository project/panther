<?php

declare(strict_types=1);

namespace Drupal\Tests\panther\FunctionalJavascript\Constraint;

use PHPUnit\Framework\Constraint\Constraint;

/**
 *
 */
final class CrawlerSelectorEnabled extends Constraint {

  /**
   * @param string $selector
   */
  public function __construct(private readonly string $selector) {}

  /**
   * @return string
   */
  public function toString(): string {
    return sprintf('selector "%s" is disabled', $this->selector);
  }

  /**
   * @param $crawler
   *
   * @return bool
   */
  protected function matches($crawler): bool {
    $xpath = sprintf(
      ".//label[text()[contains(.,'%s')]][1]/following-sibling::input[1]",
      $this->selector,
    );
    $input = $crawler->filterXPath($xpath);

    return $input->getAttribute('disabled') === null;
  }

}
