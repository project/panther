<?php

declare(strict_types=1);

namespace Drupal\Tests\panther\FunctionalJavascript;

use Drupal\Driver\DrupalDriver;

class CreatedEntityManager {

  /**
   * Managed entities.
   *
   * @var array[]
   */
  private array $entities;

  /**
   * @var \Drupal\Driver\DrupalDriver
   */
  private DrupalDriver $driver;

  /**
   * @param \Drupal\Driver\DrupalDriver $driver
   */
  public function __construct(DrupalDriver $driver) {
    $this->driver = $driver;
    $this->entities = [
      'users' => [],
      'nodes' => [],
    ];
  }

  /**
   * @param $entity
   *
   * @return void
   */
  public function addUser($entity): void {
    $this->entities['users'][] = $entity;
  }

  /**
   * @param $entity
   *
   * @return void
   */
  public function addNode($entity): void {
    $this->entities['nodes'][] = $entity;
  }

  /**
   * @return void
   */
  public function deleteEntities(): void {
    foreach ($this->entities['users'] as $user) {
      $this->driver->userDelete($user);
      $this->driver->processBatch();
    }

    foreach ($this->entities['nodes'] as $nodes) {
      $this->driver->nodeDelete((object) ['nid' => $nodes->nid]);
    }
  }

}
