<?php

declare(strict_types=1);

namespace Drupal\Tests\panther\FunctionalJavascript;

use Drupal\Tests\panther\FunctionalJavascript\Constraint\CrawlerSelectorDisabled;
use Drupal\Tests\panther\FunctionalJavascript\Constraint\CrawlerSelectorEnabled;
use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\TimeoutException;

/**
 * Provides assertions for Drupal.
 */
trait DrupalAssertionsTrait {

  /**
   * Asserts that a message contains a header and a message.
   *
   * @param string $header
   *   The header.
   * @param string $message
   *   The message.
   */
  public function assertMessageContains(string $header, string $message): void {
    try {
      $this->getClient()->waitFor('.messages-list');
      self::assertPageContains($header);
      self::assertPageContains($message);
    }
    catch (NoSuchElementException|TimeoutException $e) {
      self::fail('No message found.');
    }
  }

  /**
   * Assert that a page contains a text a specific number of times.
   *
   * @param string $text
   *   The text to search for.
   * @param int $times
   *   The number of times the text should appear.
   */
  public function assertPageContainsNTimes(string $text, int $times): void {
    self::assertEquals(
      $times,
      self::getCrawler()
        ->filterXPath('//*[normalize-space(text())="' . $text . '"]')
        ->count()
    );
  }

  /**
   * Assert that a table row contains a specific text.
   *
   * @param string $text
   *   The text to locate the row.
   * @param int $column
   *   The column to look for the text.
   * @param string $value
   *   The value to search for.
   */
  public function assertTableRowWithTextContains(string $text, int $column, string $value): void {
    $row = sprintf(
      '//table//tr[td[%d][contains(., \'%s\')]]',
      $column,
      $text
    );

    $success = FALSE;
    self::getCrawler()->filterXPath($row)->each(
      static function ($node) use ($value, &$success) {
        self::assertStringContainsString($value, $node->text());
        $success = TRUE;
      }
    );

    if (!$success) {
      self::fail(\sprintf('No row found with text "%s" in column %d.', $text, $column));
    }
  }

  /**
   * @param string $selector
   * @param string $message
   *
   * @return void
   */
  public static function assertElementDisabled(
    string $selector,
    string $message = '',
  ): void {
    self::assertThat(
      self::getCrawler(),
      new CrawlerSelectorDisabled($selector),
      $message
    );
  }

  /**
   * @param string $selector
   * @param string $message
   *
   * @return void
   */
  public static function assertElementEnabled(
    string $selector,
    string $message = '',
  ): void {
    self::assertThat(
      self::getCrawler(),
      new CrawlerSelectorEnabled($selector),
      $message
    );
  }

  /**
   * @return void
   */
  public function assertLoggedIn(): void {
    self::assertPageContains('Log out');
  }

}
