<?php

declare(strict_types=1);

use Drupal\Tests\panther\FunctionalJavascript\PantherTestCase;

class HomePageTest extends PantherTestCase {

  public function testHomePage(): void {
    $this->maximizeWindow();
    $this->goToPage('/');
    self::assertPageContains('Super easy vegetarian pasta bake');

    $this->takeScreenshots('homepage', 'test1');
  }

}
